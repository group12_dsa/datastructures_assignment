#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAX_STUDENTS 100
#define MAX_NAME_FORMAT 51 // 50 characters + 1 to cater for the null terminator
#define DATE_FORMAT 11 // YYYY-MM-DD format(10 characters)+1 to center for the null terminator
#define REGISTRATION_NUM_FORMAT 7 //6digits + 1 to cater for teh null terminator
#define PROGRAM_CODE_FORMAT 5 // 4 characters + 1 to cater for the null terminator

struct student{
    char name[MAX_NAME_FORMAT];
    char dob[DATE_FORMAT];
    char reg_num[REGISTRATION_NUM_FORMAT];
    char program_code[PROGRAM_CODE_FORMAT];
    float tuition;
};