#include <stdio.h>
#include <string.h>


void bubblesort_by_name(){
    if (num_students != 0){  
    for (int i = 0; i < num_students - 1; i++) {
        for (int j = 0; j < num_students - i - 1; j++) {
            if (strcmp(students[j].name, students[j + 1].name) > 0) {
                //swapping
                struct Student temp = students[j];
                students[j] = students[j + 1];
                students[j + 1] = temp;
            }
        }
    }printf("Students sorted by name:\n");
    for (int i = 0; i < num_students; i++){
        printf("%s\n", students[i].name);
    }
    }else{
        printf("No student found in database!!\n");
    }
}

void bubblesort_by_regno(){
    if (num_students != 0){  
    for (int i = 0; i < num_students - 1; i++) {
        for (int j = 0; j < num_students - i - 1; j++) {
            if (strcmp(students[j].reg_num, students[j + 1].reg_num) > 0) {
                //swapping
                struct Student temp = students[j];
                students[j] = students[j + 1];
                students[j + 1] = temp;
            }
        }
    }printf("Students sorted by registration number:\n");
    for (int i = 0; i < num_students; i++){
        printf("%s\n", students[i].reg_num);
    }
    }else{
        printf("No student found in database!!\n");
    }
}