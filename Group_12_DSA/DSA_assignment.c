//  ----------------Part (a)------------------
#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>

#define MAX_STUDENTS 100
#define MAX_NAME_FORMAT 51  // 50 characters + 1 to cater for the null terminator
#define DATE_FORMAT 11  // YYYY-MM-DD format(10 characters) + 1 to cater for the null terminator
#define REGISTRATION_NUM_FORMAT 7  // 6 digits + 1 to cater for the null terminator
#define PROGRAM_CODE_FORMAT 5  // 4 characters + 1 to cater for the null terminator

struct Student{
    char name[MAX_NAME_FORMAT];
    char dob[DATE_FORMAT];
    char reg_num[REGISTRATION_NUM_FORMAT];
    char program_code[PROGRAM_CODE_FORMAT];
    float tuition;
};


struct Student students[MAX_STUDENTS];
int num_students = 0;

void add_student();
void display_students();
void update_student();
void delete_student();
void search_student();
void bubblesort_by_name();
void bubblesort_by_regno();
void export_records();

// -------------Part (b)-------------

int main(){
    int option;
    // print the options on terminal
    do{
        printf("\n--------STUDENT MANAGEMENT SYSTEM_GROUP 12--------\n");
        printf("\n1. Add Student\n");
        printf("2. Display Students\n");
        printf("3. Update Student\n");
        printf("4. Delete Student\n");
        printf("5. Search Student by Registration Number\n");
        printf("6. Sort Students by name\n");
        printf("7. Sort Students by registration number\n");
        printf("8. Export Records\n");
        printf("9. Exit\n");
        printf("Enter your choice: ");
        scanf("%d", &option);

        // use cases for choosing individual tasks
        switch(option) {
            case 1:
                add_student();
                break;
            case 2:
                display_students();
                break;
            case 3:
                update_student();
                break;
            case 4:
                delete_student();
                break;
            case 5:
                search_student();
                break;
            case 6:
                bubblesort_by_name();
                break;
            case 7:
                bubblesort_by_regno();
                break;
            case 8:
                export_records();
                break;
            case 9:
                printf("Exiting program...\n");
                break;
            default:
                printf("Invalid choice. Please try again.\n");
        }
    }while(option != 9);
    return 0;
}

void add_student(){
    if (num_students < MAX_STUDENTS){
        // enter student details
        printf("Enter student's name: ");
        scanf(" %[^\n]", students[num_students].name);
        printf("Enter student's date of birth (YYYY-MM-DD): ");
        scanf("%s", students[num_students].dob);
        printf("Enter student's registration number: ");
        scanf("%s", students[num_students].reg_num);
        printf("Enter student's program code: ");
        scanf("%s", students[num_students].program_code);
        printf("Enter student's annual tuition: ");
        scanf("%f", &students[num_students].tuition);
        num_students++;
        printf("Student added successfully!\n");
    }else{
        printf("Maximum number of students reached!\n");
    }
}

void display_students() {
    if (num_students != 0){    
    printf("\nList of Students:\n\n");

    // loopthrough all current students registered to show all details
    for (int i = 0; i < num_students; i++) {
        printf("Name: %s\n", students[i].name);
        printf("Date of Birth: %s\n", students[i].dob);
        printf("Registration Number: %s\n", students[i].reg_num);
        printf("Program Code: %s\n", students[i].program_code);
        printf("Annual Tuition: shs.%.2f\n", students[i].tuition);
        printf("\n");
    }
    }else{
        printf("No student found in database!!\n");
    }
    
}

void update_student() {
    char reg_num[REGISTRATION_NUM_FORMAT];
    if (num_students != 0){    
    printf("Enter student's registration number to update: ");
    scanf("%s", reg_num);

    for (int i = 0; i < num_students; i++) {
        if (strcmp(students[i].reg_num, reg_num) == 0) {
            printf("Enter new student's name: ");
            scanf(" %[^\n]", students[i].name);
            printf("Enter new student's date of birth (YYYY-MM-DD): ");
            scanf("%s", students[i].dob);
            printf("Enter new student's new registration number: ");
            scanf("%s", students[i].reg_num);
            printf("Enter new student's program code: ");
            scanf("%s", students[i].program_code);
            printf("Enter new student's annual tuition: ");
            scanf("%f", &students[i].tuition);

            printf("Student information updated successfully!\n");
            return;
        }
    }
    printf("Student with registration number %s not found.\n", reg_num);
    }else{
        printf("No student found in database!!\n");
    }
}

void delete_student() {
    char reg_num[REGISTRATION_NUM_FORMAT];
    if (num_students != 0){    
    printf("Enter student's registration number to delete: ");
    scanf("%s", reg_num);

    for (int i = 0; i < num_students; i++) {
        if (strcmp(students[i].reg_num, reg_num) == 0) {
            // Shift elements to fill the gap
            for (int j = i; j < num_students - 1; j++) {
                students[j] = students[j + 1];
            }
            num_students--;
            printf("Student deleted successfully!\n");
            return;
        }
    }
    printf("Student with registration number %s not found.\n", reg_num);
    }else{
        printf("No student found in database!!\n");
    }
}
// -------------Part (c)-------------
void search_student(){
    char reg_num[REGISTRATION_NUM_FORMAT];
    if (num_students != 0){  
    printf("Enter student's registration number to search: ");
    scanf("%s", reg_num);
    for (int i = 0; i < num_students; i++){
        if (strcmp(students[i].reg_num, reg_num) == 0){
            printf("Student found!\n");
            printf("Name: %s\n", students[i].name);
            printf("Date of Birth: %s\n", students[i].dob);
            printf("Registration Number: %s\n", students[i].reg_num);
            printf("Program Code: %s\n", students[i].program_code);
            printf("Annual Tuition: %.2f\n", students[i].tuition);
            return;
        }
    }printf("Student with registration number %s not found.\n", reg_num);
    }else{
        printf("No student found in database!!\n");
    }
}

// -------------Part (d)-------------
void bubblesort_by_name(){
    if (num_students != 0){  
    for (int i = 0; i < num_students - 1; i++) {
        for (int j = 0; j < num_students - i - 1; j++) {
            if (strcmp(students[j].name, students[j + 1].name) > 0) {
                //swapping
                struct Student temp = students[j];
                students[j] = students[j + 1];
                students[j + 1] = temp;
            }
        }
    }printf("\nStudents sorted by name:\n");
    for (int i = 0; i < num_students; i++){
        printf("%s,%s\n", students[i].name,students[i].reg_num);
    }
    }else{
        printf("No student found in database!!\n");
    }
}

void bubblesort_by_regno(){
    if (num_students != 0){  
    for (int i = 0; i < num_students - 1; i++) {
        for (int j = 0; j < num_students - i - 1; j++) {
            if (strcmp(students[j].reg_num, students[j + 1].reg_num) > 0) {
                //swapping
                struct Student temp = students[j];
                students[j] = students[j + 1];
                students[j + 1] = temp;
            }
        }
    }printf("\nStudents sorted by registration number:\n");
    for (int i = 0; i < num_students; i++){
        printf("%s,%s\n", students[i].reg_num,students[i].name);
    }
    }else{
        printf("No student found in database!!\n");
    }
}

// -------------Part (e)-------------
void export_records() {
    if (num_students != 0) {
        // Open the file in append mode
        FILE *file = fopen("student_records.csv", "r+");
        if (file == NULL) {
            printf("Error opening file.\n");
            return;
        }

        // Check if each record already exists in the file
        bool record_exists;
        for (int i = 0; i < num_students; i++) {
            rewind(file); // Move the file pointer to the beginning
            record_exists = false;
            char line[256];
            // Read each line in the file and check for a matching record
            while (fgets(line, sizeof(line), file) != NULL) {
                if (strstr(line, students[i].reg_num) != NULL) {
                    // Record already exists, set flag and break
                    record_exists = true;
                    break;
                }
            }
            if (!record_exists) {
                // Record doesn't exist, append it to the file
                fseek(file, 0, SEEK_END); // Move the file pointer to the end
                fprintf(file, "%s,%s,%s,%s,%.2f\n", students[i].name, students[i].dob, students[i].reg_num, students[i].program_code, students[i].tuition);
            }
        }
        fclose(file);
        printf("Records exported successfully.\n");
    } else {
        printf("No student found in database!!\n");
    }
}